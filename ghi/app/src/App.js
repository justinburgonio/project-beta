import { BrowserRouter, Routes, Route,  } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import NewTechnicianForm from './NewTechnicianForm';
import TechniciansList from './TechniciansList';
import AppointmentsList from './AppointmentsList';
import NewAppointmentForm from './NewAppointmentForm';
import ServiceHistory from './ServiceHistory';
import VehicleModels from './ModelList';
import NewModelForm from './NewModelForm';
import NewManufacturerForm from './NewManufacturerForm';
import ManufacturerList from './ManufacturerList';
import AutomobileList from './AutomobileList';
import NewAutomobileForm from './NewAutomobileForm';

function App() {
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [servicehistory, setServiceHistory] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);

  const getTechnicians = async () => {
    const technicianUrl = "http://localhost:8080/api/technicians/";
    const technicianResponse = await fetch(technicianUrl);

    if (technicianResponse.ok){
      const data = await technicianResponse.json();
      const technicians = data.technicians;
      setTechnicians(technicians)
    }
  };

  const getAppointments = async () => {
    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const appointmentResponse = await fetch(appointmentUrl);

    if (appointmentResponse.ok){
      const data = await appointmentResponse.json();
      const appointments = data.appointments;
      setAppointments(appointments)
    }
  };

  const getServiceHistory = async () => {
    const servicehistoryUrl = "http://localhost:8080/api/appointments/";
    const servicehistoryResponse = await fetch(servicehistoryUrl);
    
    if (servicehistoryResponse.ok){
      const data = await servicehistoryResponse.json();
      const servicehistory = data.servicehistory;
      setServiceHistory(servicehistory)
    }
  };

  const getManufacturers = async () => {
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const manufacturerResponse = await fetch(manufacturerUrl);

    if (manufacturerResponse.ok){
      const data = await manufacturerResponse.json();
      const manufacturers = data.manufacturers;
      setManufacturers(manufacturers)
    }
  };


  const getModels = async () => {
    const modelrUrl = "http://localhost:8100/api/models/";
    const modelResponse = await fetch(modelrUrl);

    if (modelResponse.ok){
      const data = await modelResponse.json();
      const models = data.models;
      setModels(models)
    }
  };

  const getAutomobiles = async () => {
    const automobilerUrl = "http://localhost:8100/api/automobiles/";
    const automobileResponse = await fetch(automobilerUrl);

    if (automobileResponse.ok){
      const data = await automobileResponse.json();
      const automobiles = data.automobiles;
      setAutomobiles(automobiles)
    }
  };


  useEffect(() => {
    getTechnicians();
    getAppointments();
    getServiceHistory();
    getManufacturers();
    getModels();
    getAutomobiles();
  }, []);




  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians">
            <Route path="" element={<TechniciansList technicians={technicians} getTechnicians={getTechnicians} />} />
            <Route path="create" element={<NewTechnicianForm technicians={technicians} getTechnicians={getTechnicians} />} />
          </Route>
          <Route path="/appointments">
            <Route path="" element={<AppointmentsList appointments={appointments} getAppointments={getAppointments} onUpdateAppointments={setAppointments} />} />
            <Route path="create" element={<NewAppointmentForm appointments={appointments} getAppointments={getAppointments} />} />
          </Route>
          <Route path="/servicehistory" element={<ServiceHistory servicehistory={servicehistory} getServiceHistory={getServiceHistory} />} />
          <Route path="/models">
            <Route path="" element={<VehicleModels vehicleModels={models} getVehicleModels={getModels}/>} />
            <Route path="create" element={<NewModelForm vehicleModels={VehicleModels} getVehicleModels={getModels}/>} />
          </Route> 
          <Route path="/manufacturers">
            <Route path="" element={<ManufacturerList manufacturers={manufacturers} getManufacturers={getManufacturers} />} />
            <Route path="create" element={<NewManufacturerForm manufacturers={manufacturers} getManufacturers={getManufacturers} />} />
          </Route>
          <Route path="/automobiles">
            <Route path="" element={<AutomobileList automobiles={automobiles} getAutomobiles={getAutomobiles} />} />
            <Route path="create" element={<NewAutomobileForm automobiles={automobiles} getAutomobiles={getAutomobiles} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;