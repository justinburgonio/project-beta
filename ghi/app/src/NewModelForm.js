import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function NewModelForm() {
  const [name, setName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturerId, setManufacturerId] = useState('');
  const [manufacturers, setManufacturers] = useState([]);
  const navigate = useNavigate();

  const fetchManufacturers = async () => {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const newVehicleModel = {
      name,
      picture_url: pictureUrl,
      manufacturer_id: manufacturerId,
    };

    const response = await fetch('http://localhost:8100/api/models/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newVehicleModel),
    });

    if (response.ok) {
      setName('');
      setPictureUrl('');
      setManufacturerId('');
      navigate('/models');
    }
  };

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <div className="card">
            <div className="card-body">
              <h2 className="card-title">Create a Vehicle Model</h2>
              <form onSubmit={handleSubmit}>
                <div className="mb-3">
                  <input onChange={(event) => setName(event.target.value)} value={name} type="text" id="name" placeholder="Model name..." className="form-control"/>
                </div>
                <div className="mb-3">
                  <input onChange={(event) => setPictureUrl(event.target.value)} value={pictureUrl} type="text" id="pictureUrl" placeholder="Picture URL..." className="form-control"/>
                </div>
                <div className="mb-3">
                  <select onChange={(event) => setManufacturerId(event.target.value)} value={manufacturerId} id="manufacturerId" className="form-select">
                    <option value="">Choose a manufacturer</option>
                    {manufacturers.map((manufacturer) => (
                      <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    ))}
                  </select>
                </div>
                <button type="submit" className="btn btn-primary">Create Vehicle Model</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NewModelForm;
