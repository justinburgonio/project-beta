import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function NewTechnicianForm() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [employeeId, setEmployeeId] = useState("");
    const navigate = useNavigate();

    const handleFirstName = async (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastName = async (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeId = async (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const data = {
        first_name: firstName,
        last_name: lastName,
        employee_id: employeeId,
    }

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            "Content-type": "application/json",
        },
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician)
            setFirstName("");
            setLastName("");
            setEmployeeId("");
            navigate('/technicians');
        }
    };
  

    return(
        <div className="my-5 container">
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a Technician</h1>
                <form onSubmit={handleSubmit} id="create-technician-form">
                  <div className="form-floating mb-3">
                    <input
                    value={firstName}
                    onChange={handleFirstName}
                    placeholder="First Name"
                    required
                    type="text"
                    name="first name"
                    id="name"
                    className="form-control"
                    />
                    <label htmlFor="name">First Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                    value={lastName}
                    onChange={handleLastName}
                    placeholder="Last Name"
                    required
                    type="text"
                    name="last name"
                    id="last name"
                    className="form-control"
                    />
                    <label htmlFor="last name">Last Name</label>
                  </div>
                  <div className="form-floating mb-3">
                  <input
                    value={employeeId}
                    onChange={handleEmployeeId}
                    placeholder="Employee ID"
                    required
                    type="text"
                    name="employee id"
                    id="employee id"
                    className="form-control"
                  />
                  <label htmlFor="employee id">Employe ID</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
  );
}

export default NewTechnicianForm;