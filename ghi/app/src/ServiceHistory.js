import React, { useEffect, useRef, useState } from "react";

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredAppointments, setFilteredAppointments] = useState([]);
  const inputEl = useRef("");

  useEffect(() => {
    const fetchAppointments = async () => {
      const appointmentsUrl = "http://localhost:8080/api/appointments/";
      const response = await fetch(appointmentsUrl);
      const data = await response.json();
      setAppointments(data.appointments);
    };
    fetchAppointments();
  }, []);

  useEffect(() => {
    setFilteredAppointments(
      appointments.filter((appointment) =>
        appointment.vin.toLowerCase().includes(searchTerm.toLowerCase())
      )
    );
  }, [appointments, searchTerm]);

  const handleSearch = (e) => {
    e.preventDefault();
    setSearchTerm(inputEl.current.value);
  };

  return (
    <div>
      <form onSubmit={handleSearch}>
        <div className="ui search">
          <div className="ui icon input">
            <input
              ref={inputEl}
              type="text"
              placeholder="Search Vin"
              className="prompt"
            />
            <i className="search icon"></i>
          </div>
          <button type="submit">Search</button>
        </div>
      </form>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Reason</th>
            <th>Technician</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.is_vip ? "Yes" : "No"}</td>
                <td>{appointment.customer}</td>
                <td>
                  {new Date(appointment.date_time).toLocaleTimeString()}
                </td>
                <td>
                  {new Date(appointment.date_time).toLocaleDateString()}
                </td>
                <td>{appointment.reason}</td>
                <td>
                  {appointment.technician.first_name}{" "}
                  {appointment.technician.last_name}
                </td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;