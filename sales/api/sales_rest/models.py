from django.db import models

# Create your models here.



class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True, null=True)

class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True, null=True)




class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.PositiveBigIntegerField(unique=True, null=True)


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE)
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salesperson",
        on_delete=models.CASCADE)
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE)
    price = models.PositiveIntegerField()