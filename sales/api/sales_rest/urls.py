from django.urls import path
from .views import list_salespeople, delete_salespeople, list_customers, delete_customers, list_sales ,delete_sales


urlpatterns = [
    path("salespeople/", list_salespeople, name="salespeople"),
    path("salespeople/<int:id>/", delete_salespeople, name="delete_salespeople"),
    path("customers/", list_customers, name="customers"),
    path("customers/<int:id>/", delete_customers, name="delete_customers"),
    path("sales/", list_sales, name="sales"),
    path("sales/<int:id>/", delete_sales, name="delete_sales"),
]