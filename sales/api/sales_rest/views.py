from django.shortcuts import render, get_object_or_404
from .models import AutomobileVO, Salesperson, Customer, Sale
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from json import JSONEncoder
# Create your views here.


class SaleEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Sale):
            return {
                'automobile': obj.automobile.vin,
                'salesperson': obj.salesperson.employee_id,
                'customer': obj.customer.phone_number,
                'price': obj.price,
                'id': obj.id,
            }
        return super(SaleEncoder , self).default(obj)

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        'first_name',
        'last_name',
        'employee_id',
        'id',
        ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'first_name',
        'last_name',
        'address',
        'phone_number',
        'id',
        ]


# class SaleEncoder(ModelEncoder):
#     model = Sale
#     properties = [
#         'automobile',
#         'salesperson',
#         'customer',
#         'price',
#         'id',
#         ]
#     encoder = {
#         'automobile': AutomobileVO,
#         'salesperson': SalespersonEncoder,
#         'customer': CustomerEncoder,
#     }
# class AutomobileVOEncoder(ModelEncoder):
#     model = AutomobileVO
#     properties = [
#         'vin',
#         'id',
#         ]

@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def delete_salespeople(request, id):
    if request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False,
        )
    
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_customers(request, id):
    if request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0})



# @require_http_methods(["GET", "POST"])
# def list_sales(request):
#     if request.method == "GET":
#         sales = Sale.objects.all()
#         sales_list = []
#         for sale in sales:
#             sales_dict = {
#                 "automobile": sale.automobile.vin,
#                 "salesperson": sale.salesperson.employee_id,
#                 "customer": sale.customer.id,
#                 "price": sale.price,
#             }
#             sales_list.append(sales_dict)

#         return JsonResponse(
#             {"sales": sales_list},
#             # encoder=SaleEncoder,
#             safe=False,
#         )

#     else:
#         content = json.loads(request.body)
#         try:
#             automobile = AutomobileVO.objects.get(vin=content['vin'])
#             salesperson = Salesperson.objects.get(employee_id=content['salesperson'])
#             customer = get_object_or_404(Customer, id=content['customer'])
#         except (AutomobileVO.DoesNotExist, Salesperson.DoesNotExist, Customer.DoesNotExist):
#             print()
#             return JsonResponse(
#                 {"error": "Invalid data"},
#                 status=400,
#             )

#         sale = Sale.objects.create(
#             automobile=automobile,
#             salesperson=salesperson,
#             customer=customer,
#             price=content['price'],
#         )

#         sale_dict = {
#             "automobile": sale.automobile.vin,
#             "salesperson": sale.salesperson.employee_id,
#             "customer": sale.customer.id,
#             "price": sale.price,
#         }

#         return JsonResponse(
#             sale_dict,
#             sale,
#             encoder=SaleEncoder,
#             safe=False,
#         )



@require_http_methods(["DELETE"])
def delete_sales(request, id):
    if request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0})
    


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        sales_data = [SaleEncoder().default(sale) for sale in sales]
        return JsonResponse(
            {"sales": sales_data},
            safe=False,
        )

    content = json.loads(request.body)       
    try:
        automobile = AutomobileVO.objects.get(vin=content['vin'])
        salesperson = Salesperson.objects.get(employee_id=content['salesperson'])
        customer = get_object_or_404(Customer, id=content['customer'])
    except (AutomobileVO.DoesNotExist, Salesperson.DoesNotExist, Customer.DoesNotExist):
        return JsonResponse(
            {"error": "Invalid data"},
            status=400,
        )

    sale = Sale.objects.create(
        automobile=automobile,
        salesperson=salesperson,
        customer=customer,
        price=content['price'],
    )

    sale_dict = {
        "automobile": sale.automobile.vin,
        "salesperson": sale.salesperson.employee_id,
        "customer": sale.customer.id,
        "price": sale.price,
    }

    return JsonResponse(
        sale_dict,
        safe=False,
    )
    # else:
    #     content = json.loads(request.body)
    #     salesperson = Sale.objects.create(**content)
    #     return JsonResponse(
    #         salesperson,
    #         encoder=SaleEncoder,
    #         safe=False,
    #     )

# @require_http_methods(["DELETE", "GET", "PUT"])
# def list_sales(request,id):
#     if request.method == "GET":
#         try:
#             sales= Sale.objects.get(id=id)
#             return JsonResponse(
#                 sales,
#                 encoder=SaleEncoder,
#                 safe=False,
#             )
#         except Sale.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response
#     elif request.method == "DELETE":
#         try:
#             sales = Sale.objects.get(id=id)
#             sales.delete()
#             return JsonResponse(
#                 sales,
#                 encoder=SaleEncoder,
#                 safe=False,
#             )
#         except Sale.DoesNotExist:
#             return JsonResponse({"message": "Does not exist"})
#     else: # PUT
#         try:
#             content = json.loads(request.body)
#             sales = Sale.objects.get(id=id)

#             props =[
#                 "automobile"
#                 "salesperson"
#                 "customer"
#                 "price"
#                 "id"
#             ]
#             for prop in props:
#                 if prop in content:
#                     setattr(sales, prop,content[prop])
#             sales.save()
#             return JsonResponse(
#                 sales,
#                 encoder=SaleEncoder,
#                 safe=False,
#             )

#         except sales.DoesNotExist:
#             response = JsonResponse({'message': "Does Not exist"})
#             response.status_code = 404
#             return response

#  # else:
#     #     content = json.loads(request.body)
#     #     automobile = AutomobileVO.objects.create(**content)
#     #     return JsonResponse(
#     #         automobile,
#     #         encoder=AutomobileVOEncoder,
#     #         safe=False,
#     #     )

#     #     salesperson = Sale.objects.create(**content)
#     #     return JsonResponse(
#     #         salesperson,
#     #         encoder=SaleEncoder,
#     #         safe=False,
#     #     )