from django.urls import path
from .views import api_list_technician, api_show_technician, api_list_appointments, api_detail_appointments, api_cancel_appointments, api_finish_appointments

urlpatterns = [
    path('technicians/', api_list_technician, name='api_list_technician'),
    path('technicians/<int:id>/', api_show_technician, name='api_show_technician'),
    path('appointments/', api_list_appointments, name='api_list_appointments'),
    path('appointments/<int:id>/', api_detail_appointments, name='api_detail_apointments'),
    path('appointments/<int:id>/cancel/', api_cancel_appointments, name='api_cancel_apointments'),
    path('appointments/<int:id>/finish/', api_finish_appointments, name='api_finish_apointments'),
]